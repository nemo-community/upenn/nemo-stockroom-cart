# Generated by Django 3.2.19 on 2023-06-09 17:49

import django.utils.timezone
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("NEMO", "0045_version_4_5_5"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="QuantityModification",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                (
                    "modification_type",
                    models.CharField(
                        choices=[("Fulfilled order", "Fulfilled order"), ("Edited", "Edited")],
                        default=None,
                        max_length=15,
                    ),
                ),
                ("old_qty", models.PositiveIntegerField()),
                ("new_qty", models.PositiveIntegerField()),
                (
                    "date",
                    models.DateTimeField(
                        default=django.utils.timezone.now,
                        help_text="The date and time when the consumable quantity was modified.",
                    ),
                ),
                ("consumable", models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to="NEMO.consumable")),
                (
                    "modifier",
                    models.ForeignKey(
                        help_text="The user who modified the quantity of this consumable.",
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="consumable_qty_modifier",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
                (
                    "withdraw",
                    models.ForeignKey(
                        blank=True,
                        help_text="Withdrawal associated with this request",
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="NEMO.consumablewithdraw",
                    ),
                ),
            ],
            options={
                "ordering": ["-date"],
            },
        ),
        migrations.CreateModel(
            name="ConsumableThumbnail",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("image", models.ImageField(upload_to="stockroom_thumbnails/")),
                ("consumable", models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to="NEMO.consumable")),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.CreateModel(
            name="ConsumableRequest",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("quantity", models.PositiveIntegerField()),
                (
                    "date",
                    models.DateTimeField(
                        default=django.utils.timezone.now,
                        help_text="The date and time when the user placed the request for the consumable.",
                    ),
                ),
                (
                    "date_deleted",
                    models.DateTimeField(
                        default=None, help_text="The date and time when the request was marked as fulfilled.", null=True
                    ),
                ),
                ("deleted", models.BooleanField(default=False, help_text="Marks request as deleted")),
                ("consumable", models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to="NEMO.consumable")),
                (
                    "customer",
                    models.ForeignKey(
                        help_text="The user who will use the consumable item.",
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="consumable_requester",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
                (
                    "project",
                    models.ForeignKey(
                        help_text="The withdraw will be billed to this project.",
                        on_delete=django.db.models.deletion.CASCADE,
                        to="NEMO.project",
                    ),
                ),
                (
                    "withdraw",
                    models.ForeignKey(
                        blank=True,
                        help_text="Withdrawal associated with this request",
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="NEMO.consumablewithdraw",
                    ),
                ),
            ],
            options={
                "ordering": ["-date"],
            },
        ),
    ]
